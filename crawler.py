#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests


def request(url):
    """requests.get() return:
          if subdomain : 200
          if No subdomain : requests.exceptions.ConnectionError"""
    try:
        return requests.get("http://" + url)
    except requests.exceptions.ConnectionError:
        pass


target_url = "example_site.net"

with open("files-and-dirs-wordlist.txt", "r") as wordlist_file:
    # Read one line at time
    for line in wordlist_file:
        # Delete space between words
        word = line.strip()
        test_url = target_url + "/" + word
        response = request(test_url)
        if response:
            print("[+] Discovered URL --> " + test_url)
    print("[+] Analysis completed")
