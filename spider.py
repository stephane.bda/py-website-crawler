#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import urlparse

"""Recursively Discovering All Paths On a Target Website"""

target_url = "https://example_site.net"
target_links = []


def extract_link_from(url):
    response = requests.get(target_url)
    # Search for content start with href and finish with "
    return re.findall('(?:href=")(.*?)"', response.content)


def crawl(url):
    href_links = extract_link_from(url)
    for link in href_links:
        # Join url + /path
        link = urlparse.urljoin(url, link)

        if "#" in link:
            # Split link in 2 elements: before  the "#" & after the "#"
            link = link.split('#')[0]

        # ignore links not in target_url and not already in target_links
        if target_url in link and link not in target_links:
            target_links.append(link)
            print(link)
            # Recall Crawl for crawling each link found
            crawl(link)


crawl(target_url)
